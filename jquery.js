(function (window) {

    var document = window.document;

    var jQuery = function( selector, context) {
		return new jQuery.fn.init( selector, context);
    };

    jQuery.fn = jQuery.prototype = {

        init: function(selector, context, root) {
            if (!selector) {
                return this;
            }

            if (typeof selector === 'function')
                return window.onload = selector;

            if (typeof selector === 'object') {
                this[0] = selector;
                return this;
            }

            var temp = document.querySelectorAll(selector);
            for (var i = 0; i < temp.length; i++) {
                this[i] = temp[i];
            }

            this.length = temp.length;
            this.selector = selector;

            return this;
        },

        addClass: function( value){

          var rnotwhite =(/\S+/g);
          var rclass = /[\t\r\n\f]/g;
          var classes, elem, cur, curValue, clazz, j, finalValue,
      			i = 0;

          if(typeof value ==='function') {
            return this.each( function( j) {
              jQuery( this).addClass( value.call( this, j, getClass( this)));
            });
          }

          if(typeof value === "string" && value) {
      			classes = value.match( rnotwhite) || [];

      			while(( elem = this[ i++ ])) {
      				curValue = getClass( elem);
      				cur = elem.nodeType === 1 &&
      					( " " + curValue + " ").replace( rclass, " ");

      				if(cur) {
      					j = 0;
      					while(( clazz = classes[ j++ ])) {
      						if(cur.indexOf( " " + clazz + " ") < 0) {
      							cur += clazz + " ";
      						}
      					}
      				}
              finalValue = trim( cur);
    					if(curValue !== finalValue) {
    						elem.setAttribute( "class", finalValue);
    					}
      			}
      		}

          return this;

        },

        append: function( elem){

                if (typeof elem === 'function') {
                    this.each(( i, a) => {
                        a.innerHTML += `${elem(i, a)}`;
                    });
                }

                if (typeof elem === 'string') {
                    this.each(( i, a) => {
                        a.innerHTML += elem;
                    });
                }

                if (typeof elem === 'object') {

                    if( elem.length === undefined) {
                      this.each(( i, a) => {
                          a.appendChild(elem.cloneNode(true));
                      });
                    }

                    for (var j = 0; j < elem.length ; j++) {
                      var parent = elem[j].parentElement;
                      parent.removeChild(elem[j]);

                      this.each((i, a) => {
                          a.appendChild(elem[j].cloneNode(true));
                      });
                    }
                }

            return this;
        },


        html: function(value) {

            if (value !== undefined) {
                this.each(( i, a) => {
                    a.innerHTML = value;
                });

                return this;
            } else {

                return this[0].innerHTML;
            }
        },


        attr: function( name, value){

            if (name !== undefined) {
                // get attr
                if (value === undefined) {
                    var fl = false;
                    var attrValue;

                    this.each(( i, a) => {
                        if(a.getAttribute(name) !== undefined && fl === false) {
                            fl = true;
                            attrValue = a.getAttribute(name);
                        }
                    });

                    return attrValue;
                } else {
                    return this.each (( i, a) => {
                        a.setAttribute( name, value);
                    })
                }
            }

            return undefined;
        },

        children: function( elem){
            var child = new jQuery();
            child.length = 0;
            var index = 0;

            if (elem === undefined) {
                this.each (( i, a) => {
                    var temp = a.childNodes;

                    for (var i = 0; i < temp.length; i++) {
                        if (temp[i].nodeName !== '#text') {
                            child[index] = temp[i];
                            index++;
                            child.length++;
                        }
                    }
                });

            } else {
                var newSelector = this.selector + '>' + elem;
                var temp= document.querySelectorAll(newSelector);

                return temp;
            }

            return child;
        },

        css: function( propertyName, value){
            if (value === undefined) {
                if( typeof propertyName === 'object') {
                    this.each(( i, a) => {
                        for (var j in propertyName){
                            a.style[j] = propertyName[j];
                        }

                    });

                    return this;
                }

                if( typeof propertyName === 'string') {
                    var fl = false;
                    var propertyValue;

                    this.each(( i, a) => {
                        if (a.style[propertyName] !== '' && fl === false) {
                            fl = true;
                            propertyValue = a.style[propertyName];
                        }
                    });

                    return propertyValue;
                }

            }

            if(typeof propertyName === 'string' && typeof value === 'string') {
                return this.each(( i, a) => {
                    a.style[propertyName] = value;
                });
            }
        },

        data: function( key, value){

            if(key !== undefined) {
                if (value !== undefined) {

                    this.each(( i, a) => {
                        a.dataset[key] = value;
                    });

                    return this;
                } else {
                    if(typeof key == 'object') {
                        this.each(( i, a) => {
                            for (var j in key){
                                a.dataset[j] = key[j];
                            }

                        });

                        return this;
                    }
                    if(typeof key == 'string') {
                        var fl = false;
                        var keyValue;

                        this.each(( i, a) => {
                            if (a.dataset[key] !== undefined && fl === false) {
                                fl = true;
                                keyValue = a.dataset[key];
                            }
                        });

                        return keyValue;
                    }
                }
            } else {
                var temp = {};
                var element = this[0];

                for (var i in element.dataset) {
                    if (isNaN(element.dataset[i]) != true) {
                        temp[i] = parseInt(element.dataset[i])
                    } else {
                        temp[i] = element.dataset[i];
                    }
                }

                return temp;
            }
        },

        on: function( type, child,  fn) {
            if (arguments.length === 2) {
                this.each(( i, a) => {
                    a.addEventListener(arguments[0], arguments[1]);
                });
            }
            if (arguments.length === 3) {
                this.each(( i, a) => {
                    a.addEventListener(arguments[0], function(e) {
                      if(e.target.matches(child)) {
                        fn(e);
                      }
                    });
                });
            }

            return this;
        },

        one: function( type, fn){
            this.each(( i, a) => {
                a.addEventListener(arguments[0], function(e) {
                    e.target.removeEventListener(e.type, arguments.callee);

                    return fn(e);
                });
            });

            return this;
        },

        each: function( callback) {
            return _each( this, callback);
        }

    };

    jQuery.fn.init.prototype = jQuery.fn;


    function _each( obj, callback){

        for (var i = 0 ; i < obj.length; i++) {
            if(callback.call( obj[ i ], i, obj[ i ]) === false) {
                break;
            }
        }

        return obj;
    }

    function getClass( elem) {
    	return elem.getAttribute && elem.getAttribute( "class") || "";
    }

    function trim( text) {
      var rtrim = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g;

      return text == null ?
        "" :
       (text + "").replace( rtrim, "");
    }

    window.jQuery = window.$ = jQuery;
})(window);
